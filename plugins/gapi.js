import Vue from 'vue'
import VueGapi from 'vue-gapi'
import { API_KEY, CLIENT_ID, DISCOVERY_DOCS, SCOPES } from '~/constants'

Vue.use(VueGapi, {
  apiKey: API_KEY,
  clientId: CLIENT_ID,
  discoveryDocs: DISCOVERY_DOCS,
  scope: SCOPES,
})
