export const API_KEY = process.env.API_KEY
export const CLIENT_ID = process.env.CLIENT_ID
export const DISCOVERY_DOCS = ['https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest']
export const SCOPES = 'https://www.googleapis.com/auth/gmail.readonly'

